# April Solutions for Advent of code


## Usage 

Load Advent of Code for Common Lisp using

``` common-lisp
(ql:quickload "aoc")
```

Upon loading, change to the AOC package

``` common-lisp
(in-package :aoc)
```

Every years solutions are defined as an April `:space` inside of [year.lisp](./year.lisp), referring to `<year>.apl` files in the [./apl](./apl) directory. Every year defines solutions as apl functions named `day<n>`. 

In order to set the appropriate context, first use the `set-year` macro defined in [days.lisp](./days.lisp) to set the April `:space`. 

``` common-lisp
;;; set-year is a macro, which loads april code
(set-year 2015)

;;; being a macro, this will not work
(set-year *my-year*) ; error
```

Once the year is set, you can call a test a specific day for that year using the `day` macro. The `day` macro retrieves the input for the user if necessary, and passes the input to the April function. `day` takes an integer for the day in question, passing the function `day<n>` defined in `./apl/<year>.apl` the correct input for `../<year>/input-<day>.txt`

Optionally the `day` macro can be passed a body which is supplied an `input` bound by the macro, for any additional processing. 2015 day 1 requires that the first value of the input be passed to the April function. 

``` common-lisp
;;; for 2015 day 1
(day 1 (first input))
```

Another command provided to the user is the prompt command. This retrieves the prompt for the given day, and stashes it away in `../<year>/prompt-<day>.org`, translating the HTML to an org file.
``` common-lisp
(prompt 1)
```



### Jim cpt.jim.kirk@proton.me

