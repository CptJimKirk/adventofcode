 ⎕IO ← 0
 day1←{
     in ← {1-⍨') ('⍳⍵}                    ⍝ [in]put
     p1 ← +/                              ⍝ sum
     p2 ← {1+⊃⍸¯1=+\⍵}                    ⍝ first non-positive step
     (p1,p2) in ⊃⍵                        ⍝ all results
 }

 day2←{
     in ← ⍎¨∘('x'∘≠⊆⊢)¨                   ⍝ '10x20x30' ←→ 10 20 30
     p1 ← +/{+/(⌊/,2∘×)(1∘⌽×⊢)⍵}¨         ⍝ sum min + 2 × adjacent prods
     p2 ← +/(×/++/∘(2×(⊂2↑⍋)⌷⊢))¨         ⍝ sum of prod + 2 × the 2 smallest
     (p1,p2) in ⍵
 }

 day3←{
    dt ← 'v><^'                          ⍝ [d]irection [t]ravelled
    cp ← ,∘.,⍨1 ¯1                       ⍝ [c]ardinal  [p]airs of diagonal grid
    lm ← {≢∪ ⍺⍺ ⍵}                       ⍝ [l]ength of unique [m]oves
    in ← {0⍪↑cp⌷⍨⊂dt⍳⍵}                  ⍝ cardinal moves travelled table
    p1 ← +⍀ lm                           ⍝ sum expand all cp
    p2 ← {,↓+⍀⍤2⊢(2|⍳≢⍵)⊢⌸⍵} lm          ⍝ sum expand alternating groups
    (p1,p2) in ⊃⍵
 }

 day4←{
     ⎕←⍵
     md ← {'echo -n ''',⍵,''' | md5sum'} ⍝ shell [md]5sum
     ex ← {⎕SH md ⍵}                      ⍝ [ex]ecute md5sum for string ⍵
     do ← {⍺⍺ ex ⍺,⍕⍵ : ⍵ ◊ ⍺ ∇ ⍵+1}     ⍝ recurse until match is found
     fn ← {⍺∧.=(≢⍺)⍴⍵}                   ⍝ ⍺ matches first n from ⍵
     p1 ← (5⍴'0')∘fn do                  ⍝ match 5 leading 0's in [md]
     p2 ← (6⍴'0')∘fn do                  ⍝ match 6 leading 0's in [md]
     (⊃⍵) p1 1                              ⍝ works in theory, but very slow
 }


 day5←{
     p1←{nt ← 'ab' 'cd' 'pq' 'xy'
         nn ← {~∨/nt{∨/⍺⍷⍵}¨⊂⍵}
         ma ← {∨/2=/⍵}
         vc ← {3≤1⊥⍵∊'aeiou'}
         +/(nn ∧ ma ∧ vc)¨⍵}

     p2←{twilap ← (≢≠≢∘∪) ∧   ∧/∘(2≢/⊢) ⍝ at least twice no overlap
         twice  ← ∨/∘(twilap)     2,/⊢  ⍝ adjacent pairs twice with no overlap
         pal    ← ∨/∘((⌽≡⊢)¨)     3,/⊢  ⍝ palindrome
         fiar   ← ∨/∘({∧/⍵≡¨∘⊃⍵}¨)4,/⊢  ⍝ [f]our [i]n [a] [r]ow
         +/(pal ∧ (twice∨fiar))¨⍵}
     ⎕IO←1
     (p1,p2) ⍵
 }

day6←{
    M ←⊂1000 1000⍴0
    in←{M,⍨⌽⍵}{
        sf←{1↓¯5↑' '(≠⊆⊢)⍵}             ⍝ [s]plit  [f]ields
        a s h e←sf ⍵                    ⍝ [a]ction [s]tart [e]nd
        (f t)←⍎¨∘(','∘≠⊆⊢)¨s e          ⍝ [f]rom [t]il denotes a square
        ((⊃⌽a), f) ((⊃⌽h),t)}¨

    g←{ ⍝ step←⍺ ◊ M←⍵
        onoff ← ⍺⍺ ⍝ generalized solution for p1 + p2
        togl  ← ⍵⍵                       ⍝ name input functions
        range ← {{↓(⊃⍵)+⍤1⊢↑,⍳1+⊃|-/⍵}1↓¨⍵} ⍝ range of indices
        r     ← range ⍺
        ⍵[r]←0⌈$['e'=⊃⊃⍺;                ⍝ modify indices in place
            togl     ⍵[r];               ⍝ toggle the indies
            (onoff∘⍺)⍵[r]]               ⍝ set to on or off
        ⍵}

    ac ← {1⊥,⊃⍵}                         ⍝ sum total
    p1 ← ac        ({'n'=⊃⊃⍵} g ~)/      ⍝ set on = 1 / off = 0 / toggle = ~⍵
    p2 ← ac ({⍺+¯1+'f n'⍳⊃⊃⍵} g {2+⍵})/  ⍝ add on = 1 / off =¯1 / toggle =  2
    (p1,p2) in ⍵
 }


 day7←{
     sf←{{'' ◊ pint (⎕←⍵)}¨' '~⍨¨(⊂0 1 2 4)⌷¯5↑' '(≠⊆⊢) ⍵} ⍝ split file
     in←{⍵[⍒{+/(⊂'')≡¨⍵}¨⍵]} sf¨         ⍝ process input
     lu←{lk←≢ks←ACCUM[;0]                ⍝ [l]ook [u]p the name in ACCUM
         io←ks⍳⊂⍵                        ⍝ [i]ndex [o]f ⍵ in they ACCUM keys
         $[io<lk;⊃⌽ACCUM[io;];⍵]}        ⍝ return the value or ⍵
     rm←{⍺[(⍳≢⍺)~⍸((↑⍺)[;0])∊,⍵]}        ⍝ [r]e[m]ove values from ACCUM
     ps←{(x f y v)←¯4↑⍵                  ⍝ normalize input
         intp lu v: 'already matched'    ⍝ v has already been processed
         yi←lu y                         ⍝ process input
         xi←lu x
         Y←intp yi
         X←intp xi
         Y∧f≡'' : v up yi                ⍝ _ _ y → v
         Y∧x≡'' : v up (f select ,yi)    ⍝ _ f y → v
         (~X)∧Y : 'not ready'            ⍝ ? f ? → v
         X ∧ Y  : v up (f select xi yi)} ⍝ x f y → v

     ACCUM←1 2⍴'default' 0
     up←{ACCUM⍪←⍺ ⍵ ◊ ACCUM∘←ACCUM}
     ⍝ TODO: this doesn't return the answer yet
     ⍝ TODO: power operator doesn't work yet
     ⍝ To test, uncomment next 2 lines
     ⍝ x←in ⍵
     ⍝ ps¨x ⍝ paste this line 60 times or so
     sn←{_←ps¨⍵  ⍝ value of a or continue processing
         $[a←intp lu 'a';a;⍵ rm ACCUM[;0]]}⍣≡⊢

     p1←sn
     p2←{new←⍵{⍵←⍵[⍵[;3]⍳⊂,'b';2]←⍺◊↓⍵}↑⍺
         ACCUM⊢←1 2⍴'default' 0
         sn new}

     t←{(⊢,⍵⍵)∘⍺⍺⍨⍵} ⍝ (⊢,p2)∘p1⍨ in ⍵
     (p1 t p2) in ⍵

 }

 day8←{
     accum←1 2⍴'default' 0
     update←{accum⍪←(⊃⍵) (⊃⌽⍵) ◊ accum ⊢←accum}
     _←{_←{update 'new' ⍵}¨⍵ ◊ ⌽¯1↓⍵}⍣5⊢⍳5
     ≢accum

 }

 day10←{
     sh 'ls'
 }

 day20←{
    ⎕←⍵
 }

 day21←{
    ⎕←⍵
 }

 day22←{
     ⎕←⍵
 }


 day25←{
    '-'⎕VFI '3-4     :twelve   -234-626-    asdf'

 }
