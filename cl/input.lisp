(in-package #:aoc)

(defun to-array (lst &optional shape)
  (let ((s (if shape
               shape
               (list (length lst)))))
    (make-array s :initial-contents lst)))

(defun abs-path (path)
  (asdf:system-relative-pathname
   (intern (package-name *package*))
   path))

(defun temp (year ext)
  "get a temp file in the year directory"
  (abs-path (format nil "../~a/temp.~a"  year ext)))

(defun cookie-file ()
  (mapcar (lambda (x) (str:split #\: x))
          (str:lines
           (str:from-file
            (abs-path "../../aoc-cookies.txt")))))

(defun get-cookies ()
  (make-instance
   'drakma:cookie-jar
   :cookies (maplist
             (lambda (x)
               (make-instance
                'drakma:cookie
                :name (caar x)
                :value (cadar x)
                :domain ".adventofcode.com"))
            (cookie-file))))

(defun http-get->file (endpoint year day file)
  (let ((url (format nil "https://adventofcode.com/~d/day/~d~d" year day endpoint)))
    (format nil "HTTP Request... ~a" url)
    (text->file
     (drakma:http-request
      url
      :method :get
      :cookie-jar (get-cookies))
     file)
    url))

(defun html->org-file (year)
  (uiop:run-program
   (let ((html (temp year "html"))
         (org  (temp year "org")))
     (format nil "pandoc -o ~a -f html -t org ~a" org html))))

(defun parse-aoc-org (year file url)
  "extract prompt from htlm page"
  (text->file
   (let* ((text (str:from-file (temp year "org")))
          (no-header (second (str:split "<<sidebar>>" text)))
          (until-day (format nil "~{** ~a~}"
                             (rest (str:split "**" no-header))))
          (no-bottom (first (str:split "Both parts of this puzzle are complete!" until-day)))
          (no-footer (first (str:split "Answer:" no-bottom))))
     (str:join #\newline (list no-footer url)))
   file))

(defun text->file (text file)
  "write the text to a file"
  (with-open-file (output-stream file
                          :direction :output
                          :if-exists :supersede)
                  (format output-stream text)))

(defun value-for-day (year day filename endpoint)
  ""
  (let* ((folder (abs-path (format nil "../~d/" year)))
         (file (format nil filename folder day))
         (org-mode (str:contains? "org" file)))
    (flet ((get-file (f) (str:lines (str:from-file f))))

          (progn
           (ensure-directories-exist folder)
           (when (not (or org-mode (probe-file file)))
             (http-get->file endpoint year day file)))
           (when org-mode
             (let ((url (http-get->file endpoint year day (temp year "html"))))
               (html->org-file year)
               (parse-aoc-org year file url)))
           (when (not org-mode)
             (get-file file)))))

(defun prompt-for-day (year day)
  (value-for-day year day "~aprompt-~d.org" ""))

(defun input-for-day (year day)
  (value-for-day year day "~ainput-~d.txt" "/input"))
