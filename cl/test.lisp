(in-package #:aoc)

(defun year-folder ()
  (abs-path (format nil "../20~a" *year*)))

(defun file-list ()
  (str:lines
   (uiop:run-program
    (format nil "ls ~a" (year-folder))
    :output :string)))

(defun prompt-files (file-list)
  (remove-if-not
   (lambda (f) (str:contains? "prompt" f))
   file-list))

(defun get-day-fn (file-name)
  (format nil "day~a"
          (first (str:split "." (second (str:split "-" file-name))))))

(defun parse-answer (line)
  (remove #\.
          (remove #\=
                  (first (last (str:split " " line))))))

(defun read-answers (file-name)
  (let ((lines (str:lines
                (str:from-file (format nil "~a/~a" (year-folder) file-name)))))
    (mapcar #'parse-answer
            (remove-if-not
             (lambda (line) (str:contains? "Your puzzle answer was" line))
             lines))))

(defun prompt-days ()
  (mapcar (lambda (f)
            (list (read-answers f) (get-day-fn f)))
          (prompt-files (file-list))))

