(in-package #:aoc)
(defun tokens->parsed (tokens)
  (flet ((parse-int (x) (if (and (stringp x) (every #'digit-char-p x))
                            (list 1 (parse-integer x))
                            (list 0 0))))
        (let ((pints (mapcar #'parse-int tokens)))
          (vector (coerce (mapcar #'first pints) 'vector)
                  (coerce  (mapcar #'second pints) 'vector)))))

(defun string->tokens (INPUT DELIM)
  (let ((words (str:words INPUT)))
    (remove-if (lambda (x) (string= "" x))
               (apply #'append
                      (mapcar (lambda (x) (if DELIM
                                         (str:split DELIM x)
                                         (list x)))
                              words)))))

(defun dyalog-VFI (INPUT &optional DELIM)
    (tokens->parsed (string->tokens INPUT DELIM)))

(april
    ;;; add utilities to the global APRIL namespace, rather than
    ;;; importing for each year
 (with
  (:store-fun
   ("⎕VFI" (lambda (i &optional d) (dyalog-VFI i d)))
   ("⎕SH" (lambda (x)
            (uiop:run-program
             (coerce x 'string)
             :output :string)))))
 "")

(april
 (with
  (:space aoc15)
  (:store-fun
   ("⎕VFI" (lambda (i &optional d) (dyalog-VFI i d)))
   ("⎕SH" (lambda (x)
            (uiop:run-program
             (coerce x 'string)
             :output :string)))
   (intp (lambda (x) (if (numberp x) 1 0)))
   (pint (lambda (x) (if (and (stringp x) (every #'digit-char-p x) (not (string= "" x)))
                    (parse-integer x)
                    x)))
   (select (lambda  (args key)
             (let ((fns
                    (list (list "AND"    #'(lambda (x y) (logand x y)))
                          (list "OR"     #'(lambda (x y) (logior x y)))
                          (list "LSHIFT" #'(lambda (x y) (ash x  y)))
                          (list "RSHIFT" #'(lambda (x y) (ash x (- y))))
                          (list "NOT"    #'(lambda (x)   (lognot x))))))
               (apply (second (assoc key fns :test #'string=))
                      (coerce args 'list)))))))
 "")

(april
 (with
  (:space aoc16)
  (:store-fun
   (sh (lambda (x)
          (uiop:run-program
           (coerce x 'string)
           :output :string)))))
 "")

(april
 (with
  (:space aoc17)
  (:store-fun
   (lc  (lambda (x)   (string-downcase x)))))
 "")


