load 'utils.ijs'
day =. {{ read_utils_ <'../2019/',(":y),'.txt'}}
inp1 =. 0 ". > cutopen day 1
inp2 =.               day 2
ans1 =. 3317668 4973628
ans2 =. 3895705 6417

Day1 =. dyad define
    fuel =. 2-~<.@:%&3
    S1   =. +/@fuel
    S2   =. +/@,@}.@((0>.fuel)^:a:)
    (y { S1`S2)  run_utils_  x
)

Day2 =. dyad define
 ip =: ". x-.CRLF                  NB. (i)n(p)ut
 pr =: { {{((>.4%~# y),4)$ y}} ip    NB. (pr)ogram to be executed
 pc =: {{                            NB. PC = intcode computer
    r=.{{(}.x) pc (+`*@.(<:{.c)/ y {~ 1 2 { c) ((3 { c =. > {. x) }) y }}
    x (r`({.@]){~ 99 = {.>{.x) `:6 y NB. if code is 99, return first(y)
 }}

 P1 =: {{pr pc (x (1 2})y)}}         NB. Compute
 P2 =: {{                            NB. Find where result matches using
    c =. ,,(&.>/)~i.100              NB. (c)ombinations of all pairs 0-99
    +/ 100 1 *;(I. 19690720 = ;P1&y each c){c
 }}
 (12 2&P1)`P2@. y ip
)
p=:(1!:2)&2
p inp2&Day2 each i.2
