inp: read0 hsym `$"./2016/1.txt"  / read the file
prs: {(0,where","=x)_x} each      / split on ","
rmv: {x where (&/)", "<>\:x} each / remove ", " from x

/ constants
ps: rmv each prs inp        / [p]airs of direction/steps taken
gc: ((1+2*);2*)@\:til 2     / [g]roup the [c]ardinal directions 0 2 | 1 3

/ functions
d : {(2*"LR"?x)-1}          / [d]irection is either 1 or -1
f : {x each first ps}      
ts: {abs (-/)sum each st x} / [t]otal [s]teps taken 

/ results
cd: (sums f[{d x 0}]) mod 4 / [c]ardinal [d]irections
st: f[{"I"$1_x}] group cd   / [st]eps taken as integers
(+/) fx each gc



